import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {BASE_URL} from "../../app/app.module";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";


export interface Comment {
  id: number;
  blog: number;
  title: string;
  body: string;
}


@Injectable()
export class CommentProvider {
  baseUrl: string;

  constructor(public http: HttpClient) {
    console.log('Hello BlogProvider Provider');

    this.baseUrl = BASE_URL + '/comments/';
  }

  public get getComments(): Observable<Comment[]> {
    return this.http.get<Comment[]>(`${this.baseUrl}`).pipe(
      tap(result => console.log('get Comments: ' + result.length))
    );
  }

  public getCommentByFilter(filter: string): Observable<Comment[]> {
    return this.http.get<Comment[]>(`${this.baseUrl}?${filter}`).pipe(
      tap(result => console.log('get Comments: ' + result.length))
    );
  }


  public getComment(pk: number | string): Observable<Comment> {
    return this.http.get<Comment>(`${this.baseUrl}${pk}/`).pipe(
      tap(result => console.log('get Comment item: ' + result.id))
    );
  }

  public create(item: Comment): Observable<Comment>{
    return this.http.post<Comment>(`${this.baseUrl}`, item);
  }


  public remove(pk: number | string ): Observable<Comment>{
    return this.http.delete<Comment>(`${this.baseUrl}${pk}/`);
  }

}
