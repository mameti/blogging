import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import {Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {BASE_URL} from "../../app/app.module";

export interface Blog {
  id: number;
  title: string;
  image: string;
  body: string;
}

@Injectable()
export class BlogProvider {
  baseUrl: string;

  constructor(public http: HttpClient) {
    console.log('Hello BlogProvider Provider');

    this.baseUrl = BASE_URL;
  }

  public get getBlogs(): Observable<Blog[]> {
    return this.http.get<Blog[]>(`${this.baseUrl}/blogs/`).pipe(
      tap(result => console.log('get Blogs: ' + result.length))
    );
  }

  public getBlog(pk: number | string): Observable<Blog> {
    return this.http.get<Blog>(`${this.baseUrl}/blogs/${pk}/`).pipe(
      tap(result => console.log('get Blog item: ' + result.id))
    );
  }

  public create(item: Blog): Observable<Blog>{
    return this.http.post<Blog>(`${this.baseUrl}/blogs/`, item);
  }

  public update(item: Blog): Observable<Blog>{
    return this.http.put<Blog>(`${this.baseUrl}/blogs/${item.id}/`, item);
  }

  public partialUpdate(item: any): Observable<Blog>{
    return this.http.patch<Blog>(`${this.baseUrl}/blogs/${item.id}/`, item);
  }

  public remove(pk: number | string ): Observable<Blog>{

    return this.http.delete<Blog>(`${this.baseUrl}/blogs/${pk}/`);
  }



}
