import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { BlogListPage } from "../pages/blog-list/blog-list";
import { BlogDetailPage } from "../pages/blog-detail/blog-detail";
import { BlogProvider } from '../providers/blog/blog';
import { HttpClientModule } from "@angular/common/http";
import { CommentProvider } from '../providers/comment/comment';
import { AddBlogModule } from '../pages/add-blog/add-blog.module';
import { AddCommentModule } from '../pages/add-comment/add-comment.module';
import {HomePage} from "../pages/home/home";

// Rest API Server URL
export const BASE_URL = 'https://blogs.ymjproductions.ch/api/v1';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    TabsPage,
    BlogListPage,
    BlogDetailPage,
    HomePage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    AddBlogModule,
    AddCommentModule
  ],
  bootstrap: [IonicApp],
  //bootstrap: [AppComponent], // bootstrapped entry component

  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    TabsPage,
    BlogListPage,
    BlogDetailPage,HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    BlogProvider,
    CommentProvider
  ]
})
export class AppModule { }
