import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import {BlogListPage} from "../blog-list/blog-list";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = BlogListPage;
  tab2Root = AboutPage;
  tab3Root = ContactPage;

  constructor() {

  }
}
