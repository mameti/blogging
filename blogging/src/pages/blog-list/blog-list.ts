import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Blog, BlogProvider } from "../../providers/blog/blog";
import { Observable } from "rxjs";
import { BlogDetailPage } from "../blog-detail/blog-detail";
import { tap } from "rxjs/operators";
import { AddBlogContentPage } from '../add-blog/add-blog';

/**
 * Generated class for the BlogListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-blog-list',
  templateUrl: 'blog-list.html',
})
export class BlogListPage {
  public blogs$: Observable<Blog[]>;

  constructor(
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private blogData: BlogProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BlogListPage');
    this.loadBlogs();
  }

  addBlog(options) {
    let modal = this.modalCtrl.create(AddBlogContentPage, options);
    modal.present();
    modal.onDidDismiss(data => {
      if (data) {
        console.log("Data received from blog modal: ", data);
        this.loadBlogs();
      }
    });
  }

  private loadBlogs() {
    this.blogs$ = this.blogData.getBlogs.pipe(
      tap(data => console.log(data))
    );
  }

  public goToDetail(pk: number | string) {
    this.navCtrl.push(BlogDetailPage, { pk: pk });
  }

  public removeBlog(pk: number | string) {
    let alert = this.alertCtrl.create({
      title: 'Löschen',
      subTitle: 'Willst du den Datensatz ' + pk + ' wirklich löschen?',
      buttons: ['Dismiss', {
        text: 'Yes',
        handler: () => {
          this.blogData.remove(pk).subscribe(result => {
            console.log('removed', result);
            this.loadBlogs();
          })
        }
      }]
    });
    alert.present();
  }

}

