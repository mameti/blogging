import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { Observable } from "rxjs";
import { Blog, BlogProvider } from "../../providers/blog/blog";
import { Comment, CommentProvider } from "../../providers/comment/comment";
import { AddBlogContentPage } from '../add-blog/add-blog';
import { AddCommentContentPage } from '../add-comment/add-comment';
import { BlogListPage } from '../blog-list/blog-list';

/**
 * Generated class for the BlogDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-blog-detail',
  templateUrl: 'blog-detail.html',
})
export class BlogDetailPage {
  public pk: number | string;
  public blog: Blog;
  public blog$: Observable<Blog>;
  public comments$: Observable<Comment[]>;

  constructor(
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private blogData: BlogProvider,
    private commentData: CommentProvider,
    private alertCtrl: AlertController,
  ) {
  }

  ionViewDidLoad() {
    this.pk = this.navParams.get('pk');
    this.loadBlogDetails();
    this.loadComments();
    console.log('ionViewDidLoad BlogDetailPage');
  }

  private loadBlogDetails(): void {
    this.blog$ = this.blogData.getBlog(this.pk);
    this.blog$.subscribe(
      (blog) => {
        this.blog = blog;
      }
    );
  }

  private loadComments(): void {
    this.comments$ = this.commentData.getCommentByFilter('blog=' + this.pk);
  }

  editBlog(options) {
    let modal = this.modalCtrl.create(AddBlogContentPage, options);
    modal.present();
    modal.onDidDismiss(data => {
      if (data) {
        console.log("Data received from blog modal: ", data);
        this.loadBlogDetails();
      }
    });
  }

  addComment(options) {
    let modal = this.modalCtrl.create(AddCommentContentPage, options);
    modal.present();
    modal.onDidDismiss(data => {
      if (data) {
        console.log("Data received from comment modal: ", data);
        this.loadComments();
      }
    });
  }

  public deleteBlog(pk: number | string) {
    let alert = this.alertCtrl.create({
      title: 'Löschen',
      subTitle: 'Willst du den Datensatz ' + this.blog.title + ' mit der Id ' + pk + ' wirklich löschen?',
      buttons: ['Dismiss', {
        text: 'Yes',
        handler: () => {
          this.blogData.remove(pk).subscribe(result => {
            console.log('removed', result);
            this.goToList();
          })
        }
      }]
    });
    alert.present();
  }

  public goToList() {
    this.navCtrl.push(BlogListPage);
  }

}
