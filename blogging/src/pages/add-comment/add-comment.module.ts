import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddCommentContentPage } from './add-comment';

@NgModule({
  declarations: [
    AddCommentContentPage,
  ],
  entryComponents: [
    AddCommentContentPage
  ],
  imports: [
    IonicPageModule.forChild(AddCommentContentPage),
  ],
  exports: [
    AddCommentContentPage
  ]
})
export class AddCommentModule { }
