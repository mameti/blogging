import { Component } from "@angular/core";
import { Platform, NavParams, ViewController } from "ionic-angular";
import { Comment, CommentProvider } from "../../providers/comment/comment";

@Component({
  template: `
  <ion-header>
    <ion-toolbar>
      <ion-title>
        Add new blog
      </ion-title>
      <ion-buttons start>
        <button ion-button (click)="dismiss()">
          <span ion-text color="primary">Cancel</span> 
          <ion-icon name="md-close" showWhen="android, windows"></ion-icon>
        </button>
      </ion-buttons>
      <ion-buttons end>
      <button ion-button (click)="add()">
        <span ion-text color="primary">Save</span> 
        <ion-icon name="md-close" showWhen="android, windows"></ion-icon>
      </button>
    </ion-buttons>
    </ion-toolbar>
  </ion-header>
  <ion-content>
  <ion-list>

  <ion-item>
    <ion-label floating>Title</ion-label>
    <ion-input type="text" [(ngModel)]="comment.title"></ion-input>
  </ion-item>

  <ion-item>
    <ion-label floating>Text</ion-label>
    <ion-input type="text" [(ngModel)]="comment.body"></ion-input>
  </ion-item>

</ion-list>
  </ion-content>
  `
})
export class AddCommentContentPage {

  public comment: Comment = { id: 0, blog: 0, title: '', body: '' };

  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController,
    public commentProvider: CommentProvider
  ) {
    this.comment['blog'] = this.params.get('blogId');
  }

  dismiss(data: any) {
    this.viewCtrl.dismiss(data);
  }

  add(): void {
    this.commentProvider.create(this.comment)
      .subscribe(
        (created) => {
          this.dismiss('added');
        }
      )
  }
}
