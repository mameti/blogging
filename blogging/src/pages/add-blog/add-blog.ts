import { Component} from "@angular/core";
import { Platform, NavParams, ViewController } from "ionic-angular";
import { Blog, BlogProvider } from "../../providers/blog/blog";

@Component({
  template: `
  <ion-header>
    <ion-toolbar>
      <ion-title>
        <span *ngIf="!editMode">Add new blog</span>
        <span *ngIf="editMode">Edit blog</span>
      </ion-title>
      <ion-buttons start>
        <button ion-button (click)="dismiss()">
          <span ion-text color="primary">Cancel</span> 
          <ion-icon name="md-close" showWhen="android, windows"></ion-icon>
        </button>
      </ion-buttons>
      <ion-buttons end>
      <button ion-button (click)="add()">
        <span ion-text color="primary">Save</span> 
        <ion-icon name="md-close" showWhen="android, windows"></ion-icon>
      </button>
    </ion-buttons>
    </ion-toolbar>
  </ion-header>
  <ion-content>
  <ion-list>

  <ion-item>
    <ion-label floating>Title</ion-label>
    <ion-input type="text" [(ngModel)]="blog.title"></ion-input>
  </ion-item>

  <ion-item>
    <ion-label floating>Text</ion-label>
    <ion-input type="text" [(ngModel)]="blog.body"></ion-input>
  </ion-item>

</ion-list>
  </ion-content>
  `
})
export class AddBlogContentPage {

  public blog: Blog;
  public editMode = false;

  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController,
    public blogProvider: BlogProvider
  ) {
    this.blog = this.params.get('blog');
    this.editMode = this.params.get('editMode');
  }

  dismiss(data: any) {
    this.viewCtrl.dismiss(data);
  }

  add(): void {
    if (!this.editMode) {
      this.blogProvider.create(this.blog).subscribe(
        (added) => {
          this.dismiss('added');
        },
        (error) => {

        }
      );
    } else {
      this.blogProvider.update(this.blog).subscribe(
        (edited) => {
          this.dismiss('edited');
        },
        (error) => {

        }
      );
    }

  }
}
