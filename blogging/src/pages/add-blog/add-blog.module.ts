import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddBlogContentPage } from './add-blog';

@NgModule({
  declarations: [
    AddBlogContentPage,
  ],
  entryComponents: [
    AddBlogContentPage
  ],
  imports: [
    IonicPageModule.forChild(AddBlogContentPage),
  ],
  exports: [
    AddBlogContentPage
  ]
})
export class AddBlogModule { }
